-- 注意事项
	-- 1给数据库和表命名时尽量使用小写
	-- 2作为筛选条件的字符串是否区分大小写看校队规则 collate ..(utf8_general_ci不区分)
	-- 3 数据库的对象通常会用前缀加以区分: table / view / index/ function/procedure/trigger
		-- table / tb_
-- 存在hrs库 则删除hrs
drop database if exists hrs;
-- 创建hrs
create database hrs default charset utf8 collate utf8_general_ci;
 -- 报错,重新创建库(已改,不报了)
 -- create database hrs default charset utf8;
 
 use hrs;
 drop table if exists tb_emp;
 drop table if exists tb_dept;
 -- 注意删除的表不能有依赖(报错),有依赖则先删除或更改依赖项让其不依赖
 -- 这里就是员工rmp里面有部门的外部连接列,所以先删除员工
 
 -- 创建部门表
 create table tb_dept
 (
 dno int not null comment '编号',
 dname varchar(10) not null comment '名称',
 dloc varchar(20) not null comment '所在地',
 primary key (dno)
 );
 
 -- 批量插入操作
 insert into tb_dept values
	(10, '会计部', '北京'),
	(20, '研发部', '成都'),
	(30, '研发部', '重庆'),
	(40, '运维部', '深圳');

-- 创建员工表
create table tb_emp
(
eno int not null comment '员工编号',
ename varchar(20) not null comment '姓名',
job varchar(20) not null comment '员工职位',
mgr int comment '主管编号', -- 自参照完整性. 主管和员工(一对多),同时主管也是员工
sal int not null comment '员工月薪',
comm int comment '每月补贴',
dno int comment '所在部门编号',
primary key (eno)
);

-- 创建约束规则 fk_.., 外键约束tb_emp的dno(由tb_dept的dno约束)
alter table tb_emp add constraint fk_emp_dno 
foreign key (dno) references tb_dept (dno);

-- 创捷约束fk_..,表eno约束自己的mgr
-- alter table tb_emp add constraint ft_emp_mgr
-- foreign key (mgr) references tb_emp (eno);
-- 删除这个自参照(后面要用)
-- alter table tb_emp drop foreign key ft_emp_mgr;


-- 创建新列到指定位置,mgr后面.其实在可视化界面中可以直接调
alter table tb_emp add column hiredate date after mgr;
	-- first 添加到第一列
-- 删除这个列
alter table tb_emp drop column hiredate;

-- 添加数据学生表
insert into tb_emp values
	( 7800, '张三丰', '总裁', null, 9000, 1200, 20 ),
	( 2956, '乔峰', '分析师', 7800, 5000, 1500, 20 ),
	( 3988, '李莫愁', '设计师', 2956, 3500, 800, 20 ),
	( 3211, '张无忌', '程序员', 2956, 3200, null, 20 ),
	( 3233, '丘处机', '程序员', 2956, 3490, null, 20 ),
	( 3251, '张翠山', '程序员', 2956, 4090, null, 20 ),
	( 5566, '宋远桥', '会计师', 7800, 4090, 1000, 13 ),
	( 5234, '郭靖', '出纳', 5566, 2090, null, 13 ),
	( 3344, '黄蓉', '销售主管', 7800, 3000, 800, 30 ),
	( 1359, '胡一刀', '销售员', 3344,1890, 200, 30 ),
	( 4466, '苗人凤', '销售员', 3344, 2500, null, 30 ),
	( 3244, '欧阳锋', '程序员', 3988, 3290, null, 20 ),
	( 3577, '杨过', '会计', 5566, 2200, null, 13 ),
	( 3588, '朱九真', '会计', 5566, 2500, null, 13 );

-- 查询薪资最高的员工姓名和工资
select ename, max(sal) from tb_emp where mgr is not null;

-- 查询员工的姓名和年薪(月薪加补贴*12)
select ename, (sal+comm)*12 as 年薪 from tb_emp;

-- 查询有员工的部门和人数
select dname, t2.人数 from tb_dept,
(select dno, count(ename) as 人数 from tb_emp group by dno) t2
where tb_dept.dno=t2.dno;

-- 查询所有部门的名称和人数
select dname, 人数 from tb_dept t1 left join
(select dno, count(ename) as 人数 from tb_emp group by dno) t2
on t1.dno=t2.dno;

-- 查询薪水超过平均薪水的员工姓名和工资
select ename, sal from tb_emp where sal>(
select avg(sal) from tb_emp);

-- 查询薪水超过其所在部门平均薪水的员工姓名,编号,和工资
select ename, t1.dno, sal from tb_emp t1,
(select dno, avg(sal) as 平均值 from tb_emp group by dno) t2
where t1.dno=t2.dno and t1.sal>t2.平均值;

-- 查询部门中薪水最高的人姓名,工资和所在部门名称
select ename, sal, t2.dno from tb_emp t1 inner join 
(select dno, max(sal) as dnomax from tb_emp group by dno) t2
on t1.dno = t2.dno where sal = dnomax;

-- 查询主管的姓名和职位
select ename, job from tb_emp where eno in (
select mgr from tb_emp);
	-- 通常不使用 not in集合运算和distinct去重操作
	-- 可以考虑使用exists或 not exists替代集合运算和去重操作

-- 查询薪水排名4~6名的员工姓名和工资
select ename, sal from tb_emp order by sal desc limit 3,3;


-- explain 生成成执行计划
explain select eno, ename from tb_emp where eno=7800;
explain select eno, ename from tb_emp where ename='张三丰';
	-- 查看这个语句是如何查询的,一般已主键作为条件是最快的

-- 索引(index)(数据库的目录)
create index inx_emp_enmae on tb_emp(ename);
	-- 给数据enmae初建索引 inx_emp_.. 空间换取时间,加快了查询速度
	-- 缺点,会让增删改变慢.所以一般用在作为筛选条件的那个列上
-- 删除索引
create index inx_emp_enmae on tb_emp; 

-- 视图
-- 查询员工编号和... 的结果作为视图 vw_dept_...(不是那个语句的别名??)
create view vw_dept_count as
select eno, ename, dname from tb_emp t1 inner join tb_dept t2 on t1.dno=t2.dno;
	-- 相当查询的快照, 查这个语句相当与执行了 这些包裹的语句
-- 使用视图
select * from vw_dept_count;
	-- 视图一方面是查询到快照,可以简化你的查询
	-- 通过视图可以将用户的访问权限限制到指定的列上(视图上)
drop view vw_dept_count;




-- 重新定义界定符
delimiter $$
-- mysql命令行客户端，是通过分号（；）来界定一个命令是否完成的。在存储过程中，我们会多次使用到分号，但是这并不代表命令的结束，所以说我们需要使用delimiter命令来改变这个界定符。
-- 所以现在这里的查询软件(非命令行),是不用重新定义界定符的

-- (存储)过程/函数:通过把一系列的SQL语句封装到一个过程中,而且可以加上分支和循环,将来通过过程的名字直接调用过程即可,因为创建时已经提前编译了SQL语句,所以比直接执行SQL语句性能根号
-- 创建存储过程,sp_dept_avg_sal 
create procedure sp_dept_avg_sal(deptno int, out avgsal float)
begin
	select avg(sal) into avgsal from tb_emp where dno=deptno;
end$$
	-- 存储过程没有返回值,所以第二个参数 加上 out 变为输出参数
	--  .. into avgsal 将..的值付给avgsal
	-- float可以用 decimal(n,m)代替. 表示接受n为有效数字,m位小数
	
-- 将界定符返回;
delimiter ;

-- 调用储存过程
call sp_dept_avg_sal(20, @a);
select @a from dual;
	-- 变量名起名前面要加@
-- 删除存储过程
drop procedure sp_dept_avg_sal;

-- 触发器:在执行增删查改操作时可以触发其他的级联操作,但是有可能导致"锁表"现象,实际开发过程中,因该尽量避免使用触发器
-- 如: 更改公司表里面的部门,同时更改其员工的相应部门.就算不用触发器,也有其他跟安全的方法
-- 先删除原约束
alter table tb_emp drop foreign key fk_emp_dno;
-- 创建新的另一种约束
alter table tb_emp add constraint fk_emp_dno 
foreign key (dno) references tb_dept (dno)
on delete cascade on update cascade;
	-- 默认为 on delete restrict 不允许
	-- 另一种 on delete set null (意为删除或者更新后这边被约束值设为 null,注意键那时候不能有非空约束
	-- on delete cascade 级联操作; 
	-- 最好的选项是什么都不干,默认值为 不允许

-- 测试不用触发器,修改部门名称
update tb_dept set dno=13 where dno=10;



-- DCL: 授予权限(grant to) 和召回权限(revoke from)

-- 列出所有的现有用户
select user from mysql.user;

-- 创建用户
create user 'hello'@'%' identified  by '123456yu';
-- 第二常量为ip地址 127.0.1.1 或localhost(本机)  %(任意

-- 查看用户权限
-- 自己权限
show grants;
	-- root 用户有多种如, root@%或 root@localhost
	-- root@% 没有权限或收回给予别人权限
	-- 现在在此的用户为root@% 所以下面这些给予收回权限语句都是失败的,在ssh的sql里能行
-- 查看hello 权限
show grants for 'hello'@'%';



-- 给与权限 (hrs.* hrs库的所有权限)
grant all privileges on hrs.* to 'hello'@'%';
	-- all privileges on hrs的所有权限, 要创建库之类不行
	-- root 用户有多种如, root@%或 root@localhost
	-- root@% 没有权限或收回给予别人权限

-- 收回,增删改权限	
revoke insert, delete, update on hrs.* from 'hello'@'%';
	-- 只能查询,建表,删表??

-- 删除用户
drop user 'hello'@'%' ;



-- 事务(transaction) 把多个增删改查的操作做成不可分割的原子性操作,要么全都成功,要么全部失败. 
-- 如银行的转账操作(收款和付款)

-- 开启一个事务:两种方法
	-- 1 begin;   
	-- 2 start transaction;
-- 在一个事务环境中
begin;
delete from tb_emp;

-- 在 commit之前,所有的操作都可以通过 rollback 撤销
-- 然后后面可以重新开始新的语句
rollback;

-- 提交让事务的操作全都生效
commit;





 
































 
 