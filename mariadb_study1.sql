
-- 如果存在就删除学生表
drop table if exists tb_student;



-- 创建学生表
create table tb_student
(
stuid int not null comment '学号',
stuanme varchar(20) not null comment '姓名',
stusex bit default 1 comment '性别',
stubirth date comment '生日',
primary key (stuid)
);

-- 修改学生表,添加列
ALTER TABLE tb_student ADD COLUMN stuaddr VARCHAR ( 255 );

-- 修改学生表, 修改列为stuadders,并限长为511
alter table tb_student change column stuadder studders varchar(511);

-- 删除列stuadder
alter table tb_student drop column stuaddr;



-- 向学生表插入数据
insert into tb_student values (1001, '张三峰', 0, '1993-09-21', '会被实验');
-- 指定参数插入数据
insert into tb_student (stuid, stuanme) values (100, '蛤蟆皮');
insert into tb_student (stuid, stusex, stuanme) values (1002, 0, '宋宋');

-- 插入多个数据行 default 多个元组间使用逗号隔开
insert into tb_student (stuid, stuanme, stusex) values
(1003, '杨晓',  default),
(1004, '张无忌', 1),
(1000, '赵敏', 0);



-- 删除表数据
delete from tb_student where stuid=100;

-- 截断表 (删除全表) 跟全面的删库跑路
-- truncate table tb_student

-- 更新表数据
update tb_student set stuaddr='四川成都' where stuid=13 or stuid=1000;
		-- 有了关键字set约束,stuaddr的等号为赋值
		-- 后面参数可改为 stuid in (13, 1000)   还有and(此处不合适)
UPDATE tb_student set stubirth='1999-9-29' where stuid in (1000, 1002);
-- 更新两项
update tb_student set stubirth='1999-1-1', stuaddr='陕西顺德' where stuid=1003;



-- 创建学院表 
create table tb_collage
(
colid int auto_increment comment '编号，约束为自增字段',
colname varchar(31) not null comment '名称, 非空约束',
website varchar(1023) comment '网址',
primary key (colid)
);
		-- 学生和学院是两个实体
		-- 学生与学院是从属关系
		-- 重数:多对一(学生对学院), 一对多(学院对学生)
-- 建立(多对一的从属关系)关系,在多的那边加上一个列
alter table tb_student  add column colid int;
-- 这并有完全关联,只是一个列得到了更多意义

-- 给学院表添加数据,自增字段自动赋值
insert into tb_collage (colname) values
('计算机学院'),
('外国语学院'),
('金及管理学院'),
('工学院');

-- 修改多个学生的学院, between(之间) 1 and 10  1到10(<=10)之间
update tb_student set colid=2 where stuid between 1000 and 1003;

-- 修改2个学生
update tb_student set colid=1 where stuid in (13, 1004);


-- 给刚才添加的学院列,添加 外键约束(那个列是学院表的主键
alter table tb_student add constraint fk_student_colib
foreign key (colid) references tb_collage (colid);
-- 私以为等于创建规则:名为 fk_student_colib 参照了第二行表的主键

-- 创建唯一性约束
-- alter tabler tb_student add constraint wei_yi_cs
-- nuique (,); 元组里面的参数是独一无二的

-- 此时失败 因为参照完整性
update tb_student set colid=19 where stuid=1004;
















































