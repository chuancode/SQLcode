create table tb_emp
(
eno int not null comment '员工编号',
ename varchar(20) not null comment '姓名',
job varchar(20) not null comment '员工职位',
mgr int comment '主管编号', -- 自参照完整性. 主管和员工(一对多),同时主管也是员工
sal int not null comment '员工月薪',
comm int comment '每月补贴',
dno int comment '所在部门编号',
primary key (eno)
);


 create table tb_dept
 (
 dno int not null comment '编号',
 dname varchar(10) not null comment '名称',
 dloc varchar(20) not null comment '所在地',
 primary key (dno)
 );